# Programming in C++17 

## Docs

* https://infotraining.bitbucket.io/cpp-17/


## PROXY

### Proxy settings ###

* add to .profile

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=http://10.144.1.10:8080
```

## Ankieta

* https://docs.google.com/forms/d/e/1FAIpQLSd4_fZ2VUxTbZp5Jz3gxoxxy-95dqeoNfTYKSmL7fIlH0aYjg/viewform?hl=pl
