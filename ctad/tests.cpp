#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <list>

#include "catch.hpp"

using namespace std;

template <typename T>
void deduce1(T arg)
{
    puts(__PRETTY_FUNCTION__);
}

template <typename T>
void deduce2(T& arg)
{
    puts(__PRETTY_FUNCTION__);
}

void foo()
{}

TEST_CASE("Template Argument Deduction - case 1")
{
    int x = 10;
    const int cx = 10;
    int& ref_x = x;
    const int& cref_x = cx;
    int tab[10];

    deduce1(x);
    auto ax1 = x;

    deduce1(cx);
    auto ax2 = cx;

    deduce1(ref_x);
    auto ax3 = ref_x;

    deduce1(cref_x);
    auto ax4 = cref_x;

    deduce1(tab);
    auto ax5 = tab;

    deduce1(foo);
    auto ax6 = foo;
}

TEST_CASE("Template Argument Deduction - case 2")
{
    int x = 10;
    const int cx = 10;
    int& ref_x = x;
    const int& cref_x = cx;
    int tab[10];

    deduce2(x);
    auto& ax1 = x; // int&

    deduce2(cx);
    auto& a2 = cx; // const int&

    deduce2(ref_x);
    auto& ax3 = ref_x; // int&

    deduce2(cref_x);
    auto& ax4 = cref_x; // const int&

    deduce2(tab);
    auto& ax5 = tab; // int(&)[10]

    deduce2(foo);
    auto& ax6 = foo; // void(&)()
}

template <typename T>
void deduce3(T&& arg)
{
    puts(__PRETTY_FUNCTION__);
}

TEST_CASE("Template Argument Deduction - case 3")
{
    int x = 10;
    const int cx = 10;
    int& ref_x = x;
    const int& cref_x = cx;
    int tab[10];

    deduce3(x); // l-value passed -> T = int&, TArg = int&
    auto&& ax1 = x; // int&

    deduce3(10); // r-value passed -> T = int, TArg = int&&
    auto&& ax2 = 10; // int&&

    deduce3(cx);
    auto&& ax3 = cx;  // const int&

    deduce3(ref_x);
    auto&& ax4 = ref_x; // int&

    deduce3(cref_x); 
    auto&& ax5 = cref_x; // const int&

    deduce3(tab);
    auto&& ax6 = tab;  // int (&)[10]

    deduce3(foo); // void(&)()
}

template <typename T1, typename T2>
struct ValuePair
{
    T1 fst;
    T2 snd;

    ValuePair(const T1& a, const T2& b) : fst{a}, snd{b}
    {}

    ValuePair(const std::pair<T1, T2>& p) : fst{p.first}, snd{p.second}
    {}

    template <typename TArg>
    ValuePair(const TArg& arg) : fst{arg}, snd{arg}
    {}
};

// deduction guide
template <typename T1, typename T2>
ValuePair(T1, T2) -> ValuePair<T1, T2>;

ValuePair(const char*, const char*) -> ValuePair<string, string>;

template <typename TArg>
ValuePair(TArg arg) -> ValuePair<TArg, TArg>;

TEST_CASE("CTAD")
{
    int tab[10] = {};

    ValuePair<int, double> vp1{1, 3.14};

    ValuePair vp2{42, 665u}; // CTAD
    ValuePair vp3{"text"s, "text"};
    ValuePair vp4{3.14, tab};

    pair p{1, 3.14};
    ValuePair vp5{p};

    ValuePair vp6{"text", "abc"};

    ValuePair<int, int> vp7{42};
    ValuePair vp8{42};

    ValuePair vp9{vp8}; // special rule - init by copy -> ValuePair<int, int>
}

template <typename T>
struct Generic
{
    T value;

    using Type = common_type_t<T>;
    //using Type = T;

    Generic(Type v) : value{v}
    {        
    }
};

template <typename TArg>
Generic(TArg) -> Generic<enable_if_t<is_integral_v<TArg>, TArg>>;

TEST_CASE("disable CTAD")
{
    Generic g1{42};
    Generic<string> g2{"text"s};
}

template <typename T1, typename T2>
struct Aggregate
{
    T1 a;
    T2 b;
};

template <typename T1, typename T2>
Aggregate(T1, T2) -> Aggregate<T1, T2>;

TEST_CASE("aggregates + CTAD")
{
    Aggregate agg1{1, 4.44};
}

void bar(int x, double y)
{
    cout << "foo(" << x << ", " << y << ")\n";
}

TEST_CASE("CTAD in std library")
{
    SECTION("pair")
    {
        pair p1{4, "text"};

        const int x = 10;
        pair p2{5, x};
    }

    SECTION("tuple")
    {
        tuple t1{1, 3.14, "text", "abc"s};

        tuple t2{ pair{1, "text"} }; // pair -> tuple
    }
    
    SECTION("optional")
    {
        optional o1 = 4; // optional<int>

        optional o2{o1}; // optional<int> - copy contruction
    }

    SECTION("smart pointers")
    {
        unique_ptr<int> uptr{new int(13)};
        shared_ptr<int> sptr{new int(13)};

        shared_ptr sptr2 = move(uptr); // shared_ptr<int>
        weak_ptr wptr = sptr2; // weak_ptr<int>
    }

    SECTION("function")
    {
        function f = bar;
        f(1, 3.14);
    }

    SECTION("containers")
    {
        vector vec = {1, 2, 3, 4, 5};
        vector data{1, 5, 6};

        list lst = {"text"s, "abc"s};
        vector words(begin(lst), end(lst));

        array arr = {1, 5, 3, 5, 6};
    }

}