#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

vector<int> create_vector_rvo(size_t n)
{
    return vector<int>(n); // rvo
}

vector<int> create_vector_nrvo(size_t n)
{
    vector<int> vec;
    vec.reserve(n);

    for(size_t i = 0; i < n; ++i)
        vec.push_back(i);

    return vec; // named rvo
}


TEST_CASE("rvo")
{
    vector<int> data = create_vector_rvo(1'000'000);

    auto dataset = create_vector_nrvo(2'000'000); // may be elided
}

struct CopyMoveDisabled
{
    vector<int> data;

    CopyMoveDisabled(const CopyMoveDisabled&) = delete;
    CopyMoveDisabled(CopyMoveDisabled&&) = delete;
    CopyMoveDisabled& operator=(const CopyMoveDisabled&) = delete;
    CopyMoveDisabled& operator=(CopyMoveDisabled&&) = delete;
};

CopyMoveDisabled create_impossible()
{
    return CopyMoveDisabled{ {1, 2, 3} };
}

void use(CopyMoveDisabled arg)
{
    cout << "Using object with vec: " << arg.data.size() << "\n";
}

TEST_CASE("Noncopyable & nonmoveable can be returned from function")
{
    CopyMoveDisabled cmd = create_impossible();
    REQUIRE(cmd.data.size() == 3);

    use(create_impossible());
}