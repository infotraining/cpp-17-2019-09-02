#include <filesystem>
#include <iostream>
#include <fstream>
#include <iomanip>

#include "catch.hpp"

using namespace std;

namespace fs = std::filesystem;

TEST_CASE("path")
{
    auto path = fs::path{R"(/home/dev/temp)"};

    cout << path << endl;

    SECTION("appending to path")
    {
        path /= "training";
        REQUIRE(path == fs::path{R"(/home/dev/temp/training)"});

        path = path / "cpp17";
        REQUIRE(path == R"(/home/dev/temp/training/cpp17)");

        path.append("filesystem");
        REQUIRE(path == R"(/home/dev/temp/training/cpp17/filesystem)");

        SECTION("concatenating without slash")
        {
            path += "_demo";
            path.concat("_live");
            REQUIRE(path == R"(/home/dev/temp/training/cpp17/filesystem_demo_live)");
        }

        cout << path << endl;
    }

    SECTION("decomposing path to parts")
    {
        path = path / "training" / "cpp17" / "test.txt";
        REQUIRE(path == R"(/home/dev/temp/training/cpp17/test.txt)");

        std::cout
            << "root: " << path.root_name() << std::endl
            << "root dir: " << path.root_directory() << std::endl
            << "root path: " << path.root_path() << std::endl
            << "rel path: " << path.relative_path() << std::endl
            << "parent path: " << path.parent_path() << std::endl
            << "filename: " << path.filename() << std::endl
            << "stem: " << path.stem() << std::endl
            << "extension: " << path.extension() << std::endl;

        cout << endl;

        std::cout
            << "has root: " << path.has_root_name() << std::endl
            << "has root dir: " << path.has_root_directory() << std::endl
            << "has root path: " << path.has_root_path() << std::endl
            << "has rel path: " << path.has_relative_path() << std::endl
            << "has parent path: " << path.has_parent_path() << std::endl
            << "has filename: " << path.has_filename() << std::endl
            << "has stem: " << path.has_stem() << std::endl
            << "has extension: " << path.has_extension() << std::endl;
    }

    SECTION("checking if absolute or relative")
    {
        fs::path path1 = "R(training/cpp17/test.txt)";
        cout << path1 << endl;
        CHECK(path1.is_relative());

        auto path2 = path / path1;
        REQUIRE(path2.is_absolute());
    }

    SECTION("parent path")
    {
        fs::path path = R"(/home/dev/temp/training/cpp17/test.txt)";

        REQUIRE(path.parent_path() == R"(/home/dev/temp/training/cpp17)");
    }

    SECTION("modifing path")
    {
        fs::path path = R"(/home/dev/temp/training/cpp17/test.txt)";

        path.replace_filename("data");
        path.replace_extension("dat");

        REQUIRE(path == R"(/home/dev/temp/training/cpp17/data.dat)");
    }

    SECTION("converting to preferred separator")
    {
        auto path = fs::path{"Temp/training/cpp17"};

        cout << path << endl;
    }

    SECTION("iterating over path")
    {
        fs::path path = R"(/home/dev/temp/training/cpp17/test.txt)";

        cout << "iterating over: " << path << endl;
        for (const auto& path_part : path)
            cout << path_part << endl;
    }
}

class CheckingFileFixture
{
protected:
    fs::path path;
    fs::path file_path;
    std::error_code err_code;

    void create_file(const string& file_name)
    {
        ofstream out_file{file_name};
        out_file << "content";
    }

    void remove_file_object(const fs::path& path)
    {
        if (auto success = fs::remove(path, err_code); !success)
        {
            cout << "ERROR: " << err_code.message() << endl;
        }
    }

public:
    CheckingFileFixture()
    {
        auto base_path = fs::current_path();
        path = base_path / "temp";
        file_path = path / "sample.txt";

        fs::create_directory(path);

        create_file("temp/sample.txt");
    }

    ~CheckingFileFixture()
    {
        remove_file_object(file_path);
        remove_file_object(path);
    }
};

string rwx(fs::perms p)
{
    auto check = [p](fs::perms bit, char c) { return (p & bit) == fs::perms::none ? '-' : c; };

    return {
        check(fs::perms::owner_read, 'r'),
        check(fs::perms::owner_write, 'w'),
        check(fs::perms::owner_exec, 'x'),
        check(fs::perms::group_read, 'r'),
        check(fs::perms::group_write, 'w'),
        check(fs::perms::group_exec, 'x'),
        check(fs::perms::others_read, 'r'),
        check(fs::perms::others_write, 'w'),
        check(fs::perms::others_exec, 'x')};
}

TEST_CASE_METHOD(CheckingFileFixture, "checking the property of a directory")
{
    SECTION("checking if path refers to existing filesystem object")
    {
        REQUIRE(fs::exists(path, err_code));

        REQUIRE(fs::exists(file_path, err_code));
    }

    SECTION("checking a file size")
    {
        auto size = fs::file_size(file_path, err_code);

        cout << "file size of " << file_path << ": " << size << endl;
    }    

    SECTION("check permission")
    {
        auto status = fs::status(file_path, err_code);

        cout << "permissions: " << rwx(status.permissions()) << endl;
    }
}


class DirectoryIterationFixture
{
protected:
    fs::path base_path;
    std::error_code err_code;

    void create_file(const string& file_name)
    {
        ofstream out_file{file_name};
        out_file << "content";
    }

public:
    DirectoryIterationFixture()
    {
        base_path = fs::current_path() / "temp";
        auto path1 = base_path;
        auto path2 = base_path / "subdir1" / "subdir2";

        fs::create_directory(path1);
        fs::create_directories(path2);

        create_file("temp/sample1.txt");
        create_file("temp/sample2.dat");
        create_file("temp/sample3.txt");
        create_file("temp/subdir1/sample4.txt");
        create_file("temp/subdir1/sample5.dat");
        create_file("temp/subdir1/subdir2/sample6.dat");
    }

    ~DirectoryIterationFixture()
    {
        if (bool success = fs::remove_all("temp", err_code); !success)
            cout << "ERROR: " << err_code.message() << endl;
    }
};

std::string size_description(size_t size)
{
    stringstream ss;

    if (size >= 1'000'000'000)
        ss << (size / 1'000'000'000) << 'G';
    else if (size >= 1'000'000)
        ss << (size / 1'000'000) << 'M';
    else if (size >= 1'000)
        ss << (size / 1'000) << 'K';
    else
        ss << size << 'B';

    return ss.str();
}

TEST_CASE_METHOD(DirectoryIterationFixture, "iterating over a directory")
{
    SECTION("iterating with for_each")
    {
        cout << "\nContent of: " << base_path << endl;
        for(const fs::directory_entry& dir_entry : fs::directory_iterator(base_path))
        {
            cout << dir_entry.path() << endl;
        }
    }

    SECTION("recursive iterating with for_each")
    {       
        cout << "\nRecursive content of: " << base_path << endl;
        for(const fs::directory_entry& dir_entry : fs::recursive_directory_iterator(base_path))
        {
            auto path = dir_entry.path();
            auto rwx_status = rwx(dir_entry.status().permissions());
            auto size = fs::is_regular_file(dir_entry.status()) ? fs::file_size(dir_entry.path()) : 0u;
            std::cout << rwx_status << " - "
                 << setw(4) << right << size_description(size) << " - "
                 << path << endl;
        }
    }
}
