#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <optional>
#include <charconv>

#include "catch.hpp"

using namespace std;

TEST_CASE("optional")
{
    SECTION("construction")
    {
        optional<int> o1;
        REQUIRE_FALSE(o1.has_value());

        optional<int> o2 = 42;
        REQUIRE(o2.has_value());

        optional text = "abc"s;

        auto ctext = "abc";
        optional<string_view> osv{in_place, ctext, 3};
    }

    SECTION("getting value")
    {
        optional o1 = 42;
        optional<int> o2;

        SECTION("unsafe")
        {
            REQUIRE(*o1 == 42);            
            //REQUIRE(*o2); // UB 
        }

        SECTION("safe")
        {
            REQUIRE(o1.value() == 42);
            REQUIRE_THROWS_AS(o2.value(), bad_optional_access);
            REQUIRE(o2.value_or(-1) == -1);
        }
    }
}

optional<int> to_int(string_view str)
{
    int result{};

    auto start = str.data();
    auto end = start + str.size();

    if (auto [pos, error_code] = from_chars(start, end, result); error_code != std::errc{} || pos != end)
    {
        return nullopt;
    }

    return result;
}

TEST_CASE("to_int")
{
    REQUIRE(to_int("42").value() == 42);
    REQUIRE_THROWS_AS(to_int("4a2").value(), bad_optional_access);
}