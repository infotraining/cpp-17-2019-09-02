#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <variant>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("variant")
{
    variant<int, double, string> v1;

    REQUIRE(holds_alternative<int>(v1));
    REQUIRE(get<int>(v1) == 0);

    v1 = 3.14;
    v1 = "text"s;

    REQUIRE(v1.index() == 2);
    REQUIRE_THROWS_AS(get<int>(v1), bad_variant_access);

    REQUIRE(get_if<int>(&v1) == nullptr);
    REQUIRE(*get_if<string>(&v1) == "text"s);
}

struct Data
{
    vector<int> data;

    Data(std::initializer_list<int> il)
        : data(begin(il), end(il))
    {
    }
};

TEST_CASE("monostate")
{
    variant<monostate, Data, int, double> v;
    REQUIRE(holds_alternative<monostate>(v));
}

struct Evil
{
    operator int() { throw 42; }
};

struct Value
{
    float v;

    ~Value() {}
};

TEST_CASE("valueless variant")
{
    variant<Value, int> v {Value {12.f}}; // OK

    try
    {
        v.emplace<1>(Evil()); // v may be valueless
    }
    catch (...)
    {
    }

    //CHECK(v.valueless_by_exception());
    REQUIRE(holds_alternative<Value>(v));
}

struct Printer
{
    void operator()(int x) const
    {
        cout << "int: " << x << "\n";
    }

    void operator()(double x) const
    {
        cout << "double: " << x << "\n";
    }

    void operator()(const string& x) const
    {
        cout << "string: " << x << "\n";
    }

    template <typename T>
    void operator()(const vector<T>& v) const
    {
        cout << "[ ";
        for (const auto& item : v)
            cout << item << " ";
        cout << "]\n";
    }
};

template <typename... Lambdas>
struct Overload : Lambdas...
{
    using Lambdas::operator()...;
};

template <typename... Lambdas>
Overload(Lambdas...) -> Overload<Lambdas...>;

TEST_CASE("visiting variants")
{
    variant<int, double, string, vector<int>> v = vector {1, 2};

    SECTION("unsafe visitation")
    {
        if (holds_alternative<int>(v))
        {
            cout << "int: " << *get_if<int>(&v) << "\n";
        }
        else if (holds_alternative<double>(v))
        {
            cout << "double: " << *get_if<double>(&v) << "\n";
        }
    }

    Printer prn;
    visit(prn, v);

    SECTION("with lambda")
    {
        v = "text"s;

        string prefix = "#";

        auto printer = Overload {
            [prefix](int x) { cout << prefix << "int: " << x << "\n"; },
            [](double x) { cout << "double: " << x << "\n"; },
            [prefix](const string& x) { cout << prefix << "string: " << x << "\n"; },
            [](const vector<int>& v) { cout << "vec: " << v.size() << "\n"; }
        };

        visit(printer, v);
    }
}

struct ErrorCode
{
    string message;
    errc err_code;
};

[[nodiscard]] variant<string, ErrorCode> load_from_file(const string& file_name)
{
    if (file_name == "nie da sie"s)
    {
        return ErrorCode{ "blad: 13", errc::bad_file_descriptor};
    }

    return "content"s;
}

TEST_CASE("handling results or errors with variant")
{
    visit(Overload{
        [](const string& content) { cout << "File content: " << content << "\n"; }
        [](const ErrorCode& ec) { cout << "Error: " << ec.message << "\n"; }
    }, load_from_file("nie da sie"));
}
