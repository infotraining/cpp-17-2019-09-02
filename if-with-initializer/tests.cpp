#include <algorithm>
#include <iostream>
#include <mutex>
#include <numeric>
#include <queue>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("if with initializer")
{
    vector vec = {1, 2, 3, 4};

    if (auto pos = find(begin(vec), end(vec), 3); pos != end(vec))
    {
        cout << "Item " << *pos << " ahs been found!\n";
    }
    else
    {
        assert(pos == end(vec));
    }
}

TEST_CASE("use case with mutex")
{
    queue<int> q;
    mutex mtx_q;

    SECTION("Before C++17")
    {
        // code before

        {
            lock_guard<mutex> lk{mtx_q};

            if (!q.empty())
            {
                auto value = q.front();
                q.pop();
            }
        }

        // code after
    }

    SECTION("Since C++17")
    {
        if (lock_guard<mutex> lk{mtx_q}; !q.empty())
        {
            auto value = q.front();
            q.pop();
        }
    }
}