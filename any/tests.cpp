#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <any>
#include <typeinfo>
#include <map>

#include "catch.hpp"

using namespace std;

TEST_CASE("any")
{
    any a1;

    REQUIRE(a1.has_value() == false);

    a1 = 4;
    a1 = 3.14;
    a1 = "text"s;
    a1 = vector{1, 2, 3};

    SECTION("any_cast")
    {
        vector<int> vec = any_cast<vector<int>>(a1);
        REQUIRE(vec == vector{1, 2, 3});

        REQUIRE_THROWS_AS(any_cast<string>(a1), bad_any_cast);;
    }

    SECTION("any_cast with pointers")
    {
        vector<int>* ptr_vec = any_cast<vector<int>>(&a1);

        REQUIRE(ptr_vec != nullptr);
        REQUIRE(*ptr_vec == vector{1, 2, 3});

        REQUIRE(any_cast<string>(&a1) == nullptr);
    }

    SECTION("type + RTTI")
    {
        const type_info& type_desc = a1.type();

        cout << type_desc.name() << endl;
    }
}

class KeyValueDictionary
{
    map<string, any> pairs_;

public:
    optional<map<string, any>::const_iterator> insert(string key, any value)
    {
        auto [pos, ok] = pairs_.emplace(move(key), move(value));

        if (ok)
            return pos;

        return nullopt;
    }

    template <typename T>
    T& at(const string& key)
    {
        T* value = any_cast<T>(&pairs_.at(key));

        if (!value)
            throw bad_any_cast();

        return *value;
    }
};

TEST_CASE("KeyValueDictionary")
{
    KeyValueDictionary dict;

    dict.insert("name", "Jan"s);
    dict.insert("age", 42);

    REQUIRE(dict.at<int>("age") == 42);
    REQUIRE(dict.at<string>("name") == "Jan"s);

    REQUIRE_THROWS_AS(dict.at<int>("name"), bad_any_cast);
    REQUIRE_THROWS_AS(dict.at<int>("unkonw"), out_of_range);

    cout << "sizeof any: " << sizeof(any) << endl;
}