#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <string_view>
#include <optional>
#include <array>

#include "catch.hpp"

using namespace std;

TEST_CASE("string view")
{
    const char* ctext = "abc def";
    string text = "ghi jkl";

    string_view sv1 = ctext;
    REQUIRE(sv1.length() == 7);
    
    string_view sv2(ctext, 3);
    REQUIRE(sv2 == "abc"sv);

    string_view sv3 = text;
    string_view sv4(text.data(), 3);
    REQUIRE(sv4 == "ghi");
}   

template <typename Container>
void print_all(const Container& cont, string_view prefix)
{
    cout << prefix << ": [ ";
    for(const auto& item : cont)
        cout << item << " ";
    cout << "]\n";
}

TEST_CASE("printing")
{
    vector vec = {1, 2, 3, 4};

    print_all(vec, "vec");

    string prefix = "my vector";
    print_all(vec, prefix);
}

TEST_CASE("beware - unsafe code")
{
    string_view sv = "text"s; // sv has dangling pointer

    cout << sv << endl;
}

string_view get_prefix(string_view text)
{
    assert(text.size() >= 3 );

    return { text.data(), 3 };
}

TEST_CASE("get prefix")
{
    string text = "abcdef";

    auto prefix = get_prefix(text);
    cout << prefix << endl;

    prefix = get_prefix("ABCDEF"s); // after assignment prefix has dangling pointer
    cout << prefix << endl;
}

TEST_CASE("conversion from string_view -> string")
{
    auto text = "abc"sv;
    string str(text);
}

namespace Cpp20
{
    template <typename Iterator, typename Predicate>
    constexpr Iterator find_if(Iterator first, Iterator last, Predicate pred)
    {
        for(Iterator it = first; it != last; ++it)
            if (pred(*it))
                return it;
        return last;
    }
}

template <size_t N>
constexpr optional<string_view> get_id(const std::array<string_view, N>& data, string_view id)
{
    auto it = Cpp20::find_if(cbegin(data), cend(data), [id](auto sv) { return sv == id;});
    if (it != cend(data))
        return *it;
    return std::nullopt;
}

TEST_CASE("constexpr")
{
    constexpr string_view text = "abcded"sv;
    constexpr string_view alt_text(text.data() + 3, 3);

    static_assert(alt_text == "ded"sv);

    constexpr std::array ids = { "aa"sv, "bb"sv, "cc"sv, "dd"sv };

    constexpr optional opt = get_id(ids, "cc"sv);

    static_assert(opt.has_value());
    static_assert(opt.value() == "cc"sv);
}