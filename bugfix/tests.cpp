#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

TEST_CASE("auto + {}")
{
    int x1 = 10;
    int y1(10);
    int z1{10};

    auto ax1 = 10; // int
    auto ax2(10);  // int
    auto ax3{10};  // int - change in C++17
    static_assert(is_same_v<decltype(ax3), int>);
    // auto ax4{ 10, 20 }; // compiler error since C++17
    auto ax4 = {10}; // initializer_list<int>
}

void foo1() {}
void foo2() noexcept {}

TEST_CASE("noexcept as part of function type")
{
    void (*ptr)() noexcept = foo2;
    //ptr = foo1;

    void (*unsafe_ptr)() = foo1;
    unsafe_ptr = foo2;
}

template <typename F>
void call(F f1, F f2)
{
    f1();
    f2();
}

TEST_CASE("call will fail with foo1 & foo2")
{
    call(foo1, foo1);
    call(foo2, foo2);
    // call(foo1, foo2); // ERROR - deduction failed
}

class Interface
{
public:
    virtual void foo() noexcept = 0;
    virtual ~Interface() = default;
};

class Impl : public Interface
{
public:
    void foo() noexcept override // noexcept is required
    {}
};