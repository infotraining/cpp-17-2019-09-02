#include <algorithm>
#include <iostream>
#include <list>
#include <map>
#include <numeric>
#include <string>
#include <tuple>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    tuple<int, int, double> calc_stats(const vector<int>& data)
    {
        vector<int>::const_iterator min, max;
        tie(min, max) = minmax_element(data.begin(), data.end());

        //auto min_max_it = minmax_element(data.begin(), data.end());

        double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

        return make_tuple(*min, *max, avg);
    }
}

auto calc_stats(const vector<int>& data)
{
    auto [min_it, max_it] = minmax_element(data.begin(), data.end());

    double avg = accumulate(data.begin(), data.end(), 0.0) / data.size();

    return tuple(*min_it, *max_it, avg);
}

TEST_CASE("Before C++17")
{
    vector<int> data = {4, 42, 665, 1, 123, 13};

    auto [min, max, avg] = calc_stats(data);

    REQUIRE(min == 1);
    REQUIRE(max == 665);
    REQUIRE(avg == Approx(141.333));
}

auto get_coord() -> int(&)[3]
{
    static int pos[] = {1, 2, 3};

    return pos;
}

TEST_CASE("structured binding - native arrays")
{
    auto [x, y, z] = get_coord();

    REQUIRE(x == 1);
    REQUIRE(y == 2);
    REQUIRE(z == 3);

    int tab[] = {5, 6, 7};
    auto [a, b, c] = tab;
}

std::array<int, 3> get_velocity()
{
    return {1, 2, 3};
}

TEST_CASE()
{
    auto [vx, vy, vz] = get_velocity();

    REQUIRE(vx == 1);
    REQUIRE(vy == 2);
    REQUIRE(vz == 3);

    std::array tab{5, 6, 7, 8};
    auto [a, b, c, d] = tab;
}

struct Error
{
    int error_code;
    string message;
};

[[nodiscard]] Error open_file()
{
    return {13, "file not found"};
}

TEST_CASE("structure binding - struct/class")
{
    open_file();

    auto [errc, msg] = open_file();

    REQUIRE(errc == 13);
    cout << "Error: " << msg << "\n";
}

TEST_CASE("structured bindings - how it works")
{
    tuple tpl{1, 3.14, "text"s};

    auto [x, pi, msg] = tpl; // names x, pi, msg refer to copy of tpl

    get<0>(tpl) = 2;
    REQUIRE(x == 1);

    SECTION("structured binding is interpreted as")
    {
        auto unnamed_obj = tpl;

        auto& x = get<0>(unnamed_obj);
        auto& pi = get<1>(unnamed_obj);
        auto& msg = get<2>(unnamed_obj);
    }    
}

TEST_CASE("structured binding without copy")
{
    tuple tpl{1, 3.14, "text"s};

    auto& [x, pi, msg] = tpl;
    get<0>(tpl) = 2;
    REQUIRE(x == 2);

    SECTION("structured binding with ref is interpreted as")
    {
        auto& unnamed_obj = tpl;

        auto& x = get<0>(unnamed_obj);
        auto& pi = get<1>(unnamed_obj);
        auto& msg = get<2>(unnamed_obj);
    }
}

TEST_CASE("&, const, volatile & alignas")
{
    vector vec = {1, 2, 3};

    SECTION("const auto&")
    {
        const auto& [min, max, avg] = calc_stats(vec);
    }

    alignas(16) auto [errc, msg] = open_file();
}

struct Data
{
    int x;
    int& ref_x;
};

TEST_CASE("beware - names are references")
{
    int x = 10;
    int& ref_x = x;

    auto ax1 = x;
    auto ax2 = ref_x;

    SECTION("broken deduction")
    {
        Data data{x, ref_x};
        auto [ax1, ax2] = data;

        static_assert(is_same_v<int, decltype(ax1)>);
        static_assert(is_same_v<int&, decltype(ax2)>);

        ax2 = 42;
        REQUIRE(x == 42);
    }
}

TEST_CASE("use case")
{
    map<int, string> dict = { {1, "one"}, {2, "two"}, {3, "three"} };

    SECTION("iteration over map")
    {
        for(const auto& [key, value] : dict)
        {
            cout << key << " - " << value << "\n";
        }
    }

    SECTION("insert into associative container")
    {        
        if (auto [pos, ok] = dict.insert(pair(4, "four")); ok)
        {
            const auto& [key, value] = *pos;
            cout << "Item " << key << "was inserted\n";
        }
    }

    SECTION("init many vars")
    {
        auto [x, y, is_ok] = tuple(1, 3.14, true);

        list lst = {1, 2, 3};

        for(auto [index, pos] = tuple(0, lst.begin()); pos != lst.end(); ++index, ++pos)
        {
            cout << "item: " << *pos << " at index " << index << "\n";
        }
    }
}

enum Something { some, thing };
const map<int, string> something_desc = { {some, "some"}, {thing, "thing"} };

// step 1 - describe tuple-size of items
template <>
struct std::tuple_size<Something> 
{
    static constexpr size_t value = 2;
};

// step 2 - describe types of items
template <>
struct std::tuple_element<0, Something> {
    using type = int;
};

template <>
struct std::tuple_element<1, Something>
{
    using type = string;
};

// step 3 - how to get to values from object
template <size_t N>
decltype(auto) get(const Something& s)
{
    if constexpr(N == 0)
    {
        return static_cast<int>(s);
    }
    else
    {
        return something_desc.at(static_cast<int>(s));
    }
}

// removed because of constexpr if

// template <>
// decltype(auto) get<0>(const Something& s)
// {
//     return static_cast<int>(s);
// }

// template <>
// decltype(auto) get<1>(const Something& s)
// {
//     return something_desc.at(static_cast<int>(s));
// }

TEST_CASE("structurded binding - implementing tuple protocol")
{
    Something sth = some;

    const auto [value, description] = sth;

    REQUIRE(description == "some"s);
}