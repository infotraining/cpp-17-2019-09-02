#include <algorithm>
#include <array>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

template <typename T>
string convert_to_string(T value)
{
    if constexpr (is_arithmetic_v<T>)
    {
        return to_string(value);
    }
    else if constexpr (is_same_v<T, string>)
    {
        return value;
    }
    else
    {
        return string(value);
    }
}

TEST_CASE("constexpr if")
{
    SECTION("convert to string")
    {
        SECTION("from number")
        {
            REQUIRE(convert_to_string(665) == "665"s);
        }

        SECTION("string")
        {
            REQUIRE(convert_to_string("text"s) == "text"s);
        }

        SECTION("const char*")
        {
            string txt = convert_to_string("c-text");
            REQUIRE(txt == "c-text"s);
        }
    }
}

template <typename T, size_t N>
auto process_array(T (&data)[N])
{
    if constexpr (N <= 255)
    {
        cout << "Processing for small arrays" << endl;
        std::array<T, N> result;
        for (size_t i = 0; i < N; ++i)
            result[i] = data[i];

        return result;
    }
    else
    {
        cout << "Processing for large arrays" << endl;
        return std::vector<int>(std::begin(data), std::end(data));
    }
}

TEST_CASE("processing of arrays")
{
    int tab[64];
    auto data = process_array(tab);
    static_assert(is_same_v<std::array<int, 64>, decltype(data)>);

    int buffer[1024];
    auto vec = process_array(buffer);
    static_assert(is_same_v<std::vector<int>, decltype(vec)>);
}

namespace BeforeCpp17
{
    void print()
    {
        cout << "\n";
    }

    template <typename Head, typename... Tail>
    void print(const Head& head, const Tail&... tail)
    {
        cout << head << " ";
        print(tail...);
    }
}

template <typename Head, typename... Tail>
void print(const Head& head, const Tail&... tail)
{
    cout << head << " ";

    if constexpr(sizeof...(tail) > 0)
    {
        print(tail...);
    }
    else
    {
        cout << "\n";
    }    
}

TEST_CASE("variadic templates")
{
    print(1, 3.14, "text"s);
}