#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct Gadget
{
    string id;
    int usage_counter = 0;

    explicit Gadget(string id)
        : id {move(id)}
    {
        cout << "Gadget(" << this->id << " - " << this << ")\n";
    }

    Gadget(const Gadget& source)
        : id {source.id}
    {
        cout << "Gadget(cc: " << this->id << " - " << this << ")\n";
    }

    Gadget(Gadget&& source) noexcept
        : id {move(source.id)}
    {
        cout << "Gadget(mv: " << this->id << " - " << this << ")\n";
    }

    ~Gadget()
    {
        cout << "~Gadget(" << this->id << " - " << this << ")\n";
    }

    void use()
    {
        ++usage_counter;
    }

    auto reporter()
    {
        return [*this] { cout << "Report from Gadget(" << id << ")\n"; };
    }
};

TEST_CASE("lambda")
{
    {
        function<void()> reporter;
        {
            Gadget g {"mp3 player"};
            reporter = g.reporter();
        }

        reporter();
    }
    cout << "END of scope\n";
}

TEST_CASE("lambda expressions are implicitly constexpr")
{
    auto square = [](int x) { return x * x; };

    static_assert(square(8) == 64);

    int tab[square(10)];

    static_assert(size(tab) == 100);
}

namespace Cpp20
{
    template <typename Iterator, typename Predicate>
    constexpr Iterator find_if(Iterator first, Iterator last, Predicate pred)
    {
        for(Iterator it = first; it != last; ++it)
            if (pred(*it))
                return it;
        return last;
    }
}

TEST_CASE("constexpr lambda")
{
    auto square = [](int x) { return x * x; };

    constexpr std::array<int, square(3)> data = { square(1), square(2), square(4), square(8) };

    constexpr auto value = *Cpp20::find_if(begin(data), end(data), [](int x) { return x > 32; });

    cout << "Value: " << value << endl;
    static_assert(value == 64);

    auto by_factor = [value](int arg) { return arg * value; };
    static_assert(by_factor(2) == 128);

    if constexpr(by_factor(value) > 100)
    {
        cout << "Larger than 100\n";
    }
}