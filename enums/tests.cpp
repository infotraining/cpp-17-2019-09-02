#include <algorithm>
#include <iostream>
#include <numeric>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

enum class Coffee : uint8_t
{
    espresso,
    latte,
    americano
};

TEST_CASE("enums")
{
    Coffee c = Coffee::espresso;

    c = static_cast<Coffee>(2);

    underlying_type_t<Coffee> index = static_cast<underlying_type_t<Coffee>>(c);

    Coffee c2{2};
}

enum class Index : size_t
{
};

constexpr size_t to_size_t(Index i)
{
    return static_cast<size_t>(i);
}

struct Array
{
    vector<int> items;

    int& operator[](Index index)
    {
        return items[to_size_t(index)];
    }
};

TEST_CASE("using index in array")
{
    Array a {{1, 2, 3}};

    REQUIRE(a[Index {0}] == 1);
}

TEST_CASE("std::byte")
{
    SECTION("legacy code")
    {

        char legacy_byte = 65;
        legacy_byte += 4;

        cout << "byte: " << legacy_byte << endl;
    }

    SECTION("since C++17")
    {
        std::byte b1{65};
        b1 << 4;

        cout << "std::byte: " << to_integer<int>(b1) << endl;
    }
}