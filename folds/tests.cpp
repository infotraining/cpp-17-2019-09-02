#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

namespace BeforeCpp17
{
    template <typename Head>
    auto sum(Head h)
    {
        return h;
    }

    template <typename Head, typename... Tail>
    auto sum(Head h, Tail... t)
    {
        return h + sum(t...);
    }
}

template <typename... Args>
auto sum(Args... args)
{
    return (... + args); // left unary fold
}

template <typename... Args>
auto sum_right(Args... args)
{
    return (args + ...); // right unary fold
}

TEST_CASE("fold expressions")
{
    auto result = sum(1, 2, 3, 4, 5);
    REQUIRE(result == 15);
}

template <typename... Args>
void print(const Args&... args)
{
    bool is_first = true;

    auto with_spaces = [&is_first](const auto& arg) {
        if (!is_first)
            cout << " ";
        is_first = false;
        return arg;
    };

    (cout << ... << with_spaces(args)) << "\n"; // left binary fold
}

template <typename... Args>
void print_lines(const Args&... args)
{
    ((cout << args << "\n"), ...);
}

TEST_CASE("print with fold expressions")
{    
    print(1, 3.14, "text"s, "abc");

    print_lines(1, 3.14, "text"s, "abc");
}