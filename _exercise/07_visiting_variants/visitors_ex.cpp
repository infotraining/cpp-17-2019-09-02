#include <iostream>
#include <string>
#include <variant>
#include <vector>

#include "catch.hpp"

using namespace std;

struct Circle
{
    int radius;
};

struct Rectangle
{
    int width, height;
};

struct Square
{
    int size;
};

template <typename... Ts>
struct Overload : Ts...
{
    using Ts::operator()...;
};

template <typename... Ts>
Overload(Ts...) -> Overload<Ts...>;

TEST_CASE("visit a shape variant and calculate area")
{
    using Shape = variant<Circle, Rectangle, Square>;

    vector<Shape> shapes = {Circle{1}, Square{10}, Rectangle{10, 1}};

    double total_area{};

    auto area_calculator = Overload {
        [&total_area](const Circle& c) { total_area += 3.14 * c.radius * c.radius; },
        [&total_area](const Rectangle& r) { total_area += r.height * r.width; },
        [&total_area](const Square& s) { total_area += s.size * s.size; }
    };

    for(const auto& s : shapes)
        visit(area_calculator, s);

    REQUIRE(total_area == Approx(113.14));
}