#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

class Customer
{
private:
    std::string first_;
    std::string last_;
    uint8_t age_;

public:
    Customer(std::string f, std::string l, uint8_t age)
        : first_(std::move(f))
        , last_(std::move(l))
        , age_(age)
    {
    }

    std::string first() const
    {
        return first_;
    }

    std::string last() const
    {
        return last_;
    }

    uint8_t age() const
    {
        return age_;
    }
};

template <>
struct std::tuple_size<Customer> : std::integral_constant<int, 3>
{};

template <>
struct std::tuple_element<0, Customer> : common_type<const string&>
{};

template <>
struct std::tuple_element<1, Customer> : common_type<const string&>
{};

template <>
struct std::tuple_element<2, Customer> : common_type<uint8_t>
{};

template <size_t N>
decltype(auto) get(const Customer&);

template <>
decltype(auto) get<0>(const Customer& c)
{
    return c.first();
}

template <>
decltype(auto) get<1>(const Customer& c)
{
    return c.last();
}

template <>
decltype(auto) get<2>(const Customer& c)
{
    return c.age();
}


TEST_CASE("")
{
    Customer c("Jan", "Kowalski", 42);

    auto [f, l, a] = c;

    REQUIRE(f == "Jan");
    REQUIRE(l == "Kowalski");
}