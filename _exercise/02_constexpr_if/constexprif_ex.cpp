#include <iostream>
#include <vector>
#include <list>
#include <iterator>

#include "catch.hpp"

using namespace std;

struct GenericImpl {};
struct RandomAccessIterImpl {};


template <typename Iterator>
auto advance_it(Iterator& it, size_t n)
{
    using iter_category_tag = typename iterator_traits<Iterator>::iterator_category;

    if constexpr(is_same_v<iter_category_tag, random_access_iterator_tag>)
    {
        it += n;
        return RandomAccessIterImpl{};
    }
    else
    {
        for(size_t i = 0; i < n; ++i)
            ++it;
        return GenericImpl{};
    }
}

TEST_CASE("constexpr-if with iterator categories")
{
    SECTION("random_access_iterator")
    {
        vector<int> data = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        auto it = data.begin();

        RandomAccessIterImpl result = advance_it(it, 3);

        REQUIRE(*it == 4);
    }

    SECTION("input_iterator")
    {
        list<int> data = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        auto it = data.begin();

        RandomAccessIterImpl result = advance_it(it, 3);

        REQUIRE(*it == 4);
    }
}