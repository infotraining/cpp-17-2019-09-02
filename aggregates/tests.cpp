#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <future>

#include "catch.hpp"

using namespace std;

struct SimpleAggregate
{
    int a;
    double b;
    int tab[3];
    string name;

    void print() const
    {
        cout << "SimpleAggregate{ " << a << ", " << b << ", [ ";
        for (const auto& item : tab)
            cout << item << " ";
        cout << "], " << name << "}\n";
    }
};

TEST_CASE("aggregates")
{
    SimpleAggregate agg1{1, 3.14, {1, 2, 3}, "agg1"};
    agg1.print();

    SimpleAggregate agg2{2, 6.28};
    agg2.print();

    SimpleAggregate agg3{3, 8.44, {1} };
    agg3.print();
}

template <typename T, size_t N>
struct Array
{
    T elems_[N];
};

TEST_CASE("arrays are aggregates")
{
    int tab[3] = {1, 2}; // {1, 2, 0}
    std::array<int, 3> arr = { 1, 2 }; // {1, 2, 0}    
}

struct ComplexAggregate : SimpleAggregate, std::string
{
    vector<int> vec;
};

TEST_CASE("Complex aggregate")
{    
    ComplexAggregate agg1{ {1, 3.14, {1, 2}, "simple agg"}, {"text"}, {665, 667} };

    agg1.print();
    REQUIRE(agg1.size() == 4);
    REQUIRE(agg1.vec.size() == 2);

    static_assert(is_aggregate_v<ComplexAggregate>);
}

struct WTF
{
    int value;

    WTF() = delete; // user declared    
};

TEST_CASE("WTF")
{
    WTF wtf{};

    REQUIRE(wtf.value == 0);
}

void save_to_file(const string& filename)
{
    cout << "Start save to: " << filename << endl;
    this_thread::sleep_for(1s);
    cout << "Saved to: " << filename << endl;
}

TEST_CASE("WTF2")
{
    auto f1 = async(launch::async, save_to_file, "1");
    auto f2 = async(launch::async, save_to_file, "2");
    auto f3 = async(launch::async, save_to_file, "3");
}
